from datetime import datetime
import json
import os, sys
f = open(os.path.dirname(__file__) + "/timezones.json", encoding = 'utf-8')
timezones = json.loads(f.readline())
f.close()

def main():
    next = get_next_420()
    print(next.minutes)

class Somewhere:
  def __init__(self, minutes, places):
    self.minutes = minutes
    self.places = places

def get_utc():
    now = datetime.utcnow()
    return now.hour * 3600 + now.minute * 60

def get_420_offset():
    offset = ((16 * 3600) + (20 * 60)) - get_utc()
    if offset < 0:
        offset += 24 * 3600;
    return offset

def get_next_420():
    offset = get_420_offset()
    for timezone in timezones:
        if timezone[0] <= offset:
            return Somewhere(int((offset - timezone[0]) / 60), timezone[1])
    
    return Somewhere(666, "Uwu couldn't find it")


if __name__== "__main__" :
    main()

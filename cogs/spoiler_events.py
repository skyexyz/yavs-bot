from discord.ext import commands
from discord import File, Message
import logging
import re
import requests
import json
import io
import os

from utils import get_guild_settings, save_guild_settings, is_spoiler_channel

SPOILER_CHANNELS_KEY = 'spoiler-channels'

message_link = "https://discordapp.com/channels/{}/{}/{}"


async def repost(message):
    files = [await a.to_file(spoiler=True) for a in message.attachments]
    await message.delete()
    await message.channel.send("From <@{}>: {}".format(str(message.author.id), message.content), files=files)


class SpoilerEvents(commands.Cog, name="Spoiler and settings"):
    def __init__(self, bot, api_key):
        self.bot = bot
        self.api_key = api_key

    @commands.command(
        help="Spoiler content uploaded with this command",
        brief="Spoiler content uploaded with this command"
    )
    async def spoiler(self, ctx) -> None:
        ctx.message.content = ctx.message.content[len(">spoiler"):]
        if "https://tenor.com" in ctx.message.content:
            digits = re.search(r"(\d+)", ctx.message.content)
            try:
                gif = self.download_gif(digits.group(1))

                file = File(gif, digits.group(1) + ".gif", spoiler=True)
                text = "From <@{}>: ||{}||".format(str(ctx.message.author.id), ctx.message.content)
                await ctx.message.channel.send(text, file=file)
                await ctx.message.delete()
            except TypeError:
                pass
        else:
            await self.repost(ctx.message)

    @commands.command(name="set-channel")
    @commands.has_guild_permissions(manage_guild=True)
    async def set_channel(self, ctx, channel_name) -> None:
        guild_settings = get_guild_settings(ctx.guild.id)
        guild_settings[channel_name] = ctx.channel.id
        save_guild_settings(ctx.guild.id, guild_settings)
        await ctx.message.add_reaction("✅")

    @commands.command(
        name="spoiler-channel",
        help="Sets the current channel to auto spoiler all images an gifs",
        brief="Spoilers all content in current channel"
    )
    @commands.has_guild_permissions(manage_guild=True)
    async def spoiler_channel(self, ctx):
        guild_settings = get_guild_settings(ctx.guild.id)
        if SPOILER_CHANNELS_KEY not in guild_settings:
            guild_settings[SPOILER_CHANNELS_KEY] = []
        if ctx.channel.id in guild_settings[SPOILER_CHANNELS_KEY]:
            guild_settings[SPOILER_CHANNELS_KEY].remove(ctx.channel.id)
            await ctx.channel.send('Gifs and attachments will no longer be spoilered')
        else:
            guild_settings[SPOILER_CHANNELS_KEY].append(ctx.channel.id)
            await ctx.channel.send('Gifs and attachments will now be automatically spoilered')
        save_guild_settings(ctx.guild.id, guild_settings)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        guild_settings = get_guild_settings(payload.guild_id)
        if payload.guild_id is None:
            return  # Reaction is on a private message
        if payload.emoji.name == "❌":
            channel = self.bot.get_channel(payload.channel_id)
            message = await channel.fetch_message(payload.message_id)
            user_id = re.search(r"^From <@(\d+)>", message.content)
            if user_id and str(user_id.group(1)) == str(payload.user_id):
                await message.delete()
                logging.info("Deleting message as requested by author")
                return
        if "reaction" not in guild_settings or "discussion" not in guild_settings:
            return
        if payload.emoji.name == "❗" and payload.channel_id == guild_settings["reaction"]:
            channel = self.bot.get_channel(guild_settings["discussion"])
            link = message_link.format(payload.guild_id, payload.channel_id, payload.message_id)
            await channel.send("Member <@{}> raised an issue about {}".format(str(payload.user_id), link))

        return payload

    @commands.Cog.listener()
    async def on_message(self, message: Message) -> None:
        if message.author.id == self.bot.user.id:
            return
        if not is_spoiler_channel(message):
            return
        if len(message.attachments) > 0:
            for attachment in message.attachments:
                if not attachment.is_spoiler():
                    logging.info("Deleting non-spoilered attachment from {}".format(str(message.author)))
                    await repost(message)
                    return
        if "https://tenor.com" in message.content:
            digits = re.search(r"(\d+)", message.content)
            try:
                gif = self.download_gif(digits.group(1))

                file = File(gif, digits.group(1) + ".gif", spoiler=True)
                text = "From <@{}>: ||{}||".format(str(message.author.id), message.content)
                await message.channel.send(text, file=file)
                await message.delete()
            except TypeError:
                pass

    def download_gif(self, gif_id) -> io.BytesIO:
        response = requests.get("https://api.tenor.co/v1/gifs?ids={}&api_key={}".format(gif_id, self.api_key))
        results = json.loads(response.text)
        url = results.get("results")[0].get("media")[0].get("gif").get("url")
        response = requests.get(url)
        return io.BytesIO(response.content)

    async def repost(self, message) -> None:
        files = [await a.to_file(spoiler=True) for a in message.attachments]
        await message.delete()
        await message.channel.send("From <@{}>: {}".format(str(message.author.id), message.content), files=files)


def setup(bot) -> None:
    api_key = os.environ.get("TENOR_API_KEY")
    bot.add_cog(SpoilerEvents(bot, api_key))

from discord import TextChannel, File
import io
from discord.ext import commands


class Archive(commands.Cog):
    @commands.command(
        name='archive',
        help="Archive given channel to given archive channel",
        brief="Archive channel"
    )
    async def archive(self, ctx, *args) -> None:
        try:
            archive_channel = await commands.TextChannelConverter().convert(ctx, args[0])
        except:
            return await ctx.send("Archive channel Not Found")
        try:
            channel = await commands.TextChannelConverter().convert(ctx, args[1])
        except:
            return await ctx.send("Channel Not Found")
        # await channel.send("Archiving channel and creating logs.\nThis may take a while if the channel is big.")

        messages = []
        async for message in channel.history(limit=None):
            for a in message.attachments:
                message.content += f" (attachment: \"{a.filename}\" {a.url})"
            messages.append("[{}] {}#{} ({}): {}\n".format(message.created_at.strftime("%Y %b %d %H:%M:%S"),
                                                           message.author.name,
                                                           message.author.discriminator,
                                                           message.author.id,
                                                           message.content))
        messages.reverse()
        txt_file = io.StringIO(f"Transcript of channel #{channel.name}.\n\n" + "\n".join(messages))
        await archive_channel.send(f"Archive of #{channel.name}", file=File(txt_file, f"{channel.name}.txt"))


def setup(bot):
    bot.add_cog(Archive())

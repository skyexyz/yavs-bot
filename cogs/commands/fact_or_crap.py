from datetime import datetime

from discord.ext import commands

from utils import get_guild_settings, save_guild_settings

FACT_OR_CRAP_KEY = "fact_or_crap"


class FactOrCrap(commands.Cog):
    @commands.command(
        name='crap',
        help='Vote crap',
    )
    async def crap(self, ctx):
        self.setFactOrCrap(ctx.guild.id, ctx.author.id, False)
        await ctx.message.add_reaction("✅")

    @commands.command(
        name='fact',
        help='Vote fact',
    )
    async def fact(self, ctx):
        self.setFactOrCrap(ctx.guild.id, ctx.author.id, True)
        await ctx.message.add_reaction("✅")

    @staticmethod
    def setFactOrCrap(guild_id: str, user_id: str, fact: bool) -> None:
        guild_settings = get_guild_settings(guild_id)
        if FACT_OR_CRAP_KEY not in guild_settings:
            guild_settings[FACT_OR_CRAP_KEY] = {}
        today = datetime.today().strftime('%Y-%m-%d')
        if today not in guild_settings[FACT_OR_CRAP_KEY]:
            guild_settings[FACT_OR_CRAP_KEY][today] = {}
        guild_settings[FACT_OR_CRAP_KEY][today][user_id] = fact
        save_guild_settings(guild_id, guild_settings)


def setup(bot):
    bot.add_cog(FactOrCrap())

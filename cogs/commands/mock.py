from random import randint

from discord.ext import commands


class Mock(commands.Cog):
    @commands.command(
        name='mock',
        help='Repeat something, mOCKInGLy',
    )
    async def mock(self, ctx, *text):
        """Convert arguments into mocked form and reply."""
        await ctx.send(self.mocked(' '.join(text)))

    @staticmethod
    def mocked(message: str) -> str:
        """Convert a message into mocking form."""
        message = message.lower().replace('l', 'L').replace('i', 'I')
        return ''.join(c.upper() if randint(0, 1) else c for c in message)


def setup(bot):
    bot.add_cog(Mock())

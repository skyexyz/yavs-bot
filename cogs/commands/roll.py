import re

from discord.ext import commands
from random import randint


def dice_roll(num_of_dice, dice_type, modifier) -> int:
    total = 0
    for dice in range(0, num_of_dice):
        total += randint(1, dice_type)

    total += modifier

    return total


class Roll(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        help="Roll die of your choice",
        brief="Roll die of your choice"
    )
    async def roll(self, ctx, text) -> None:
        roll = re.search(r'(\d+)?d(\d+)([\+\-]\d+)?', text)
        if roll is None:
            await ctx.send("{} rolls {}. Result: 42069 :poop:".format(ctx.message.author, text))
            return

        num_of_dice = 1
        if roll.group(1) is not None:
            num_of_dice = int(roll.group(1))
        modifier = 0
        if roll.group(3) is not None:
            modifier = int(roll.group(3))

        dice_type = int(roll.group(2))

        if num_of_dice > 1000:
            num_of_dice = 1000

        await ctx.send("{} rolls {}d{}{}. Result: {}".format(ctx.message.author, num_of_dice, dice_type,
                                                             (roll.group(3) if roll.group(3) is not None else ""),
                                                             dice_roll(num_of_dice, dice_type, modifier)))


def setup(bot) -> None:
    bot.add_cog(Roll(bot))

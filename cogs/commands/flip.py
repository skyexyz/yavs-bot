import random

from discord.ext import commands

class Flip(commands.Cog):
    flips = [
        '(╯°□°）╯︵ ┻━┻',
        '(┛◉Д◉)┛彡┻━┻',
        '(ﾉ≧∇≦)ﾉ ﾐ ┸━┸',
        '(ノಠ益ಠ)ノ彡┻━┻',
        '(╯ರ ~ ರ）╯︵ ┻━┻',
        '(┛ಸ_ಸ)┛彡┻━┻',
        '(ﾉ´･ω･)ﾉ ﾐ ┸━┸',
        '(ノಥ,_｣ಥ)ノ彡┻━┻',
        '(┛✧Д✧))┛彡┻━┻',
        '(╯°□°）╯︵ (\\ . 0 .)\\',
    ]

    unflips = [
        '┬─┬ノ( ◕◡◕ ノ)',
        '┯━┯ノ(º₋ºノ)',
        '┬─┬ ノ( ゜-゜ノ)',
        '┬───┬ ノ༼ຈل͜ຈノ༽',
        '┬─┬ ノ( ^_^ノ)',
    ]

    @commands.command(
        name='flip',
        help='Flip a f&*#ing table!',
    )
    async def flip(self, ctx, *text):
        """Flip a random table"""
        await ctx.send(random.choice(self.flips))
    
    @commands.command(
        name='unflip',
        help='Unflip a f&*#ing table!',
    )
    async def unflip(self, ctx, *text):
        """Unflip a random table"""
        await ctx.send(random.choice(self.unflips))

def setup(bot):
    bot.add_cog(Flip())
